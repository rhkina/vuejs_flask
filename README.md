# Full stack with Vuejs+Flask+MariaDB


# Backend
Simple but complete REST API example using Flask, Flask-RESTPlus and MariaDB as database.


## Install Flask, Flask_RESTPlus, MySQL connector

### Flask
Follow Flask installation instructions described on Flask [documentation](https://flask.palletsprojects.com/en/1.1.x/%5D%28https://flask.palletsprojects.com/en/1.1.x/). 
It is highly recommended to use a virtual environment to manage the dependencies for your project, both in development and in production. See more about virtual environments in Flask [documentation](https://flask.palletsprojects.com/en/1.1.x/%5D%28https://flask.palletsprojects.com/en/1.1.x/).
[Anaconda](https://www.anaconda.com/) has a great visual way to manage Python environments.

### Flask_RESTPlus
FFlask-RESTPlus provides a coherent collection of decorators and tools to describe developer's API and expose its documentation properly using [Swagger](https://swagger.io/).
Install Flask_RESTPlus using the following **pip** command:
```sh
$ pip install flask-restplus
```

### MySQL connector
Install Python MySQL connector:
```sh
$ pip install mysql-connector-python
```

## Install MariaDB and create the Tasks table

Even though MariaDB is a fork of MySQL, these two database management systems are still quite different:

-   MariaDB is fully GPL licensed while MySQL takes a dual-license approach.
-   Each handle thread pools in a different way.
-   MariaDB supports a lot of different storage engines.
-   In many scenarios, MariaDB offers improved performance.

Python MySQL connector works both on MySQL and MariaDB databases.
[Download](https://mariadb.org/download/) the proper MariaDB version and install it following the [documentation instructions](https://mariadb.com/kb/en/binary-packages/).

Create a MariaDB table named Tasks with the following columns:

|Field name   |Type              |Null |Default |Extra          | 
|-------------|------------------|-----|--------|---------------|
|id           |int(11) (primary) |No   |None    |Auto increment |
|title        |text              |Yes  |Null    |               |
|comment      |text              |Yes  |Null    |               |

Add some data in the Tasks table through command line or some database UI (like [MySQL Workbench](https://www.mysql.com/products/workbench/) or [phpMyAdmin](https://www.phpmyadmin.net/))



## References
### Backend
- Flask-RESTPlus Full example. [online] Available at: [https://flask-restplus.readthedocs.io/en/stable/example.html](https://flask-restplus.readthedocs.io/en/stable/example.html)
- Flask-RESTPlus documentation. [online] Available at: [https://flask-restplus.readthedocs.io/en/stable/index.html](https://flask-restplus.readthedocs.io/en/stable/index.html)
- AGARWAL, Tanmay et al - Databases through Python-Flask and MariaDB (pdf). [online] Available at [http://tlc.iith.ac.in/img/gvv_tanmay_durga_database.pdf](http://tlc.iith.ac.in/img/gvv_tanmay_durga_database.pdf)


