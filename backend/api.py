from flask import Flask, jsonify
from flask_restplus import Resource, Api, fields
import mysql.connector as mariadb
import json

app = Flask(__name__)
api = Api(app, version='1.0', title='Todo API',
    description='A simple Todo API'
)

ns = api.namespace('api', description='TODO operations')

todo = api.model('Todo', {
    'id': fields.Integer(readonly=True, description='The task unique identifier'),
    'title': fields.String(required=True, description='The task title'),
    'comment': fields.String(required=False, description='The task details')
})


user = 'root'
password = '170767'
host = 'localhost'
database = 'db_tasks'
table = 'tasks'

conn = mariadb.connect(user=user, password=password, host=host, database=database)

class TodoDAO(object):
    def get_all(self):
        cur = conn.cursor()
        sql = "SELECT * from " + table
        cur.execute(sql)
        rv = cur.fetchall()
        return jsonify(rv)
    
    def get(self,id):
        cur = conn.cursor()
        try:
            sql = "SELECT * from " + table + " where id = " + str(id)
            cur.execute(sql)
            rv = cur.fetchone()
            return jsonify(rv)
        except:
            return jsonify({'Error':"Couldn't find task!"})

    def create(self, data):
        task = data
        task_title = task.get('title')
        task_comment = task.get('comment')
        print (task_title, task_comment)
        cur = conn.cursor()
        try:
            sql = "INSERT INTO " + table + " (title, comment) VALUES ('" + task_title + "', '" + task_comment + "')"
            cur.execute(sql)
            conn.commit()
            return jsonify({'title': task_title, 'comment': task_comment})
        except:
            return jsonify({'Error:':'Could not insert your task'})

    def update(self, id, data):
        task = data
        task_title = str(task.get('title'))
        task_comment = str(task.get('comment'))
        cur = conn.cursor()
        try:
            sql = "UPDATE " + table + " SET id = "+ "'" + str(id) + "'," + "title = " + "'" + task_title + "'," " comment = " + "'" + task_comment + "'" + " WHERE " + "id=" + str(id)
            print (sql)
            cur.execute(sql)
            conn.commit()
            sql = "SELECT * from " + table + " where id = " + str(id)
            cur.execute(sql)
            rv = cur.fetchone()
            return jsonify(rv)
        except:
            return jsonify({'Error:':'Could not insert your task'})

    def delete(self, id):
        cur = conn.cursor()
        try:
            sql = "DELETE FROM " + table + " WHERE id = " + str(id)
            cur.execute(sql)
            return jsonify({'Message': 'Task '+str(id)+' deleted!'})
        except:
            return jsonify({'Error':"Couldn't find task!"})


DAO = TodoDAO()

resource_fields = ns.model('Resource', {'title': fields.String, 'comment': fields.String})

@ns.route('/tasks')
class TaskList(Resource):
    @ns.doc('list_tasks')
    def get(self):
        return DAO.get_all()
    
    @ns.doc('create_task')
    @ns.expect(resource_fields)
    def post(self):
        return DAO.create(ns.payload)

@ns.route('/task/<int:id>')
@ns.response(404, 'Task not found')
@ns.param('id', 'The task identifier')
class Task(Resource):
    @ns.doc('get_task')
    def get(self, id):
        return DAO.get(id)
    
    @ns.doc('update_task')
    @ns.expect(resource_fields)
    def put(self, id):
        return DAO.update(id, ns.payload)

    @ns.doc('delete_task')
    @ns.response(204, 'Task deleted')
    def delete(self,id):
        return DAO.delete(id)


if __name__ == '__main__':
    app.run(debug=True)
    